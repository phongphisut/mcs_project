#!/usr/bin/python3

import pymysql
from datetime import datetime

dateTimeObj = datetime.now()
# timestampStr = dateTimeObj.strftime("%d-%b-%Y (%H:%M:%S.%f)")
#
# print('Current Timestamp : ', timestampStr)
# print (dateTimeObj)


# Open database connection
db = pymysql.connect("localhost","root","","mcs_project" )

# prepare a cursor object using cursor() method
cursor = db.cursor()

# license_plate = 'xx9999'
# province = 'bangkok'

def ins_db (car_status, license_plate, province) :

   # sql = """INSERT INTO car_hist(car_status, car_time,
   #    license_plate, province)
   #    VALUES (%s, %s, %s, %s)"""

   sql = """INSERT INTO car_hist(car_status, car_time,
      license_plate, province)
      VALUES (%s, 2019-11-09 22:22:22, %s, %s)"""

   recordTuple = (car_status, license_plate, province)
   # cursor.execute(sql, recordTuple)


   try:
      # Execute the SQL command
      cursor.execute(sql,recordTuple)
      # Commit your changes in the database
      db.commit()
      print("db ins success")
   except:
      # Rollback in case there is any error
      db.rollback()
      # print(sql)
      print("error insert to db")

   return
