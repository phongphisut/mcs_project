
<?php
	$link = mysqli_connect("localhost", "root", "", "mcs_project");

	if (!$link) {
		echo "Error: Unable to connect to MySQL." . PHP_EOL;
		echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
		echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
		exit;
	}

	//echo "Success: A proper connection to MySQL was made! The my_db database is great." . PHP_EOL;
	//echo "Host information: " . mysqli_get_host_info($link) . PHP_EOL;

    // mysqli_close($link);
    

    // $form_data = array(
    //     'first_name' => $first_name,
    //     'last_name' => $last_name,
    //     'email' => $email,
    //     'address1' => $address1,
    //     'address2' => $address2,
    //     'address3' => $address3,
    //     'postcode' => $postcode,
    //     'tel' => $tel,
    //     'mobile' => $mobile,
    //     'website' => $website,
    //     'contact_method' => $contact_method,
    //     'subject' => $subject,
    //     'message' => $message,
    //     'how_you_found_us' => $how_you_found_us,
    //     'time' => time()
    // );



    function dbRowInsert($table_name, $form_data, $link)
    {
        // retrieve the keys of the array (column titles)
        $fields = array_keys($form_data);

        // build the query
        $sql = "INSERT INTO ".$table_name."
        (`".implode('`,`', $fields)."`)
        VALUES('".implode("','", $form_data)."')";

        // run and return the query result resource
        return mysqli_query($link, $sql);
    }


    // the where clause is left optional incase the user wants to delete every row!
    function dbRowDelete($table_name, $where_clause='')
    {
        // check for optional where clause
        $whereSQL = '';
        if(!empty($where_clause))
        {
            // check to see if the 'where' keyword exists
            if(substr(strtoupper(trim($where_clause)), 0, 5) != 'WHERE')
            {
                // not found, add keyword
                $whereSQL = " WHERE ".$where_clause;
            } else
            {
                $whereSQL = " ".trim($where_clause);
            }
        }
        // build the query
        $sql = "DELETE FROM ".$table_name.$whereSQL;

        // run and return the query result resource
        return mysqli_query($link, $sql);
    }

    // again where clause is left optional
    function dbRowUpdate($table_name, $form_data, $where_clause='')
    {
        // check for optional where clause
        $whereSQL = '';
        if(!empty($where_clause))
        {
            // check to see if the 'where' keyword exists
            if(substr(strtoupper(trim($where_clause)), 0, 5) != 'WHERE')
            {
                // not found, add key word
                $whereSQL = " WHERE ".$where_clause;
            } else
            {
                $whereSQL = " ".trim($where_clause);
            }
        }
        // start the actual SQL statement
        $sql = "UPDATE ".$table_name." SET ";

        // loop and build the column /
        $sets = array();
        foreach($form_data as $column => $value)
        {
            $sets[] = "`".$column."` = '".$value."'";
        }
        $sql .= implode(', ', $sets);

        // append the where statement
        $sql .= $whereSQL;

        // run and return the query result
        return mysqli_query($link, $sql);
    }