-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 04, 2019 at 09:45 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mcs_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `car`
--

CREATE TABLE `car` (
  `id` int(11) NOT NULL,
  `car_id` int(10) NOT NULL,
  `license_plate` varchar(50) NOT NULL,
  `province` varchar(255) NOT NULL,
  `color` varchar(50) NOT NULL,
  `brand` varchar(100) NOT NULL,
  `date_add` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `car`
--

INSERT INTO `car` (`id`, `car_id`, `license_plate`, `province`, `color`, `brand`, `date_add`) VALUES
(1, 1, 'กข999', 'กรุงเทพฯ', 'black', 'toyota', '2019-09-03 02:56:45'),
(2, 2, 'กข888', 'นนทบุรี', 'red', 'benz', '2019-09-03 02:57:49');

-- --------------------------------------------------------

--
-- Table structure for table `car_hist`
--

CREATE TABLE `car_hist` (
  `id` int(10) NOT NULL,
  `car_in` varchar(50) NOT NULL,
  `car_out` datetime NOT NULL,
  `license_plate` varchar(10) NOT NULL,
  `province` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `car_hist`
--

INSERT INTO `car_hist` (`id`, `car_in`, `car_out`, `license_plate`, `province`) VALUES
(1, 'tair', '0000-00-00 00:00:00', '', ''),
(2, 'sdfsdfsf', '2019-09-05 00:00:00', '', ''),
(3, 'test', '2019-09-18 00:00:00', 'กข9999', 'กรุงเทพฯ'),
(7, 'fdsfsfssf', '2019-09-05 11:18:06', 'กข9999', 'กรุงเทพฯ'),
(8, 'fdsfsfssf', '2019-09-05 11:18:59', 'กข9999', 'กรุงเทพฯ'),
(9, 'dsfdsfdsf', '2019-09-06 11:06:41', 'กข999', 'กรุงเทพฯ'),
(10, 'dsfdsfdsf', '2019-09-06 11:06:41', 'กข999', 'กรุงเทพฯ'),
(11, 'tair', '0000-00-00 00:00:00', '', ''),
(12, 'sdfsdfsf', '2019-09-05 00:00:00', '', ''),
(13, 'test', '2019-09-18 00:00:00', 'กข9999', 'กรุงเทพฯ'),
(14, 'fdsfsfssf', '2019-09-05 11:18:06', 'กข9999', 'กรุงเทพฯ'),
(15, 'fdsfsfssf', '2019-09-05 11:18:59', 'กข9999', 'กรุงเทพฯ'),
(16, 'dsfdsfdsf', '2019-09-06 11:06:41', 'กข999', 'กรุงเทพฯ'),
(17, 'dsfdsfdsf', '2019-09-06 11:06:41', 'กข999', 'กรุงเทพฯ');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `l_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(10) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `f_name`, `l_name`, `username`, `email`, `password`, `create_at`, `update_at`) VALUES
(1, 'phongphisut', 'teerachai', 'phongphisut', 't.phongphisut@gmail.com', 'test1234', '2019-09-03 02:47:11', '2019-09-02 17:00:00'),
(7, 'phongphisut', 'teerachai', 'phongphisut', '', '', '2019-12-04 07:43:38', '2019-12-04 07:43:38'),
(8, 'phongphisut', 'teerachai', 'phongphisut', '', '', '2019-12-04 07:46:12', '2019-12-04 07:46:12'),
(9, 'phongphisut', 'teerachai', 'phongphisut', '', '', '2019-12-04 07:52:32', '2019-12-04 07:52:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `car`
--
ALTER TABLE `car`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_hist`
--
ALTER TABLE `car_hist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `car`
--
ALTER TABLE `car`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `car_hist`
--
ALTER TABLE `car_hist`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
